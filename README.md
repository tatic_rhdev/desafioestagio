![Desafio Tatic] (logo-tatic.png =150x76 "Desafio Tatic Estágio")

## Desafio Tatic

O objetivo deste desafio é desenvolver dois programas de linha de comando. O primeiro, o "armazenador", deverá processar um arquivo-texto de entrada armazenando-o da forma mais otimizada possível. O segundo, o "buscador", deverá pesquisar os dados processados  pelo "armazenador" com o melhor tempo de resposta possível. Os dois programas, "armazenador" e "buscador",  deverão ser concebidos de forma a alcançar, da melhor forma possível, dois objetivos aparentemente opostos, armazenar os dados da forma mais otimizada possível e pesquisar da forma mais rápida possível.

Embora esse desafio tenha o objetivo de avaliar seus conhecimentos para a vaga, esse tipo de atividade será parte do seu trabalho na Tatic!

No arquivo *sample.txt* você encontrará uma lista de registros como no trecho abaixo:
 
```
	20170219124557428;BFD99205;553254424704
	20170206175709744;B5079387;553290673006
	20170219045142081;BE987D97;553156348437
	20170209151545876;0A23C8B5;553140270338
```

No arquivo as colunas são separadas por ";" e as linhas por *line feed*. A primeira coluna representa a data de um evento (4 dígitos para o ano, 2 para o mês, 2 para o dia, 2 para as horas em formato 24h, 2 para os minutos, 2 para os segundos e 3 para os milissegundos), a coluna 2 é um identificador numérico hexadecimal de um evento e a última coluna é um identificador numérico do usuário que gerou o evento.


---

## Observações

+ O programa de armazenamento e o programa de busca não precisam ter interface gráfica mas precisam prover modo de execução por linha de comando. 
+ Poderá ser usado qualquer algoritmo que julgar necessário. É também permitido usar quaisquer bibliotecas/tecnologias de uso não comercial.
+ O programa armazenador precisa receber pelo menos um parâmetro que é o arquivo a ser armazenado (fique à vontade para acrescentar outros parâmetros caso faça sentido para sua solução). O programa buscador precisa saber buscar os dados pelas colunas de data e identificador do evento (não é necessário filtrar pelo identificador do usuário). O buscador receberá como argumentos data inicial (**>=**), data final (**<=**) e uma lista de identificadores de evento (a lista é opcional, se não informado é porque deseja-se buscar somente pelo intervalo de datas, ou seja, desejamos como resultado todos os eventos do intervalo). 

Se espera algo do tipo:
  
  
```console
	$ armazenador sample.txt
	$
	$ buscador 20170206175709744 20170219124557428 B5079387 
	$ 20170206175709744;B5079387;553290673006
	$
	$ buscador 20170206175709744 20170219124557428 BFD99205 B5079387 0A23AFD0
	$ 20170206175709744;B5079387;553290673006
	$ 20170219124557428;BFD99205;553254424704
	$
	$ buscador 20170206175709744 20170219124557428 
	$ 20170206175709744;B5079387;553290673006
	$ 20170209151545876;0A23C8B5;553140270338
	$ 20170219045142081;BE987D97;553156348437
	$ 20170219124557428;BFD99205;553254424704
```
  
+ Pode ser usada qualquer linguagem de programação aberta. Em caso de proficiência em Java ou C/C++ se recomenda o uso dessas.
+ Sua entrega deve acompanhar explicações claras de como compilar, instalar e usar os programas de armazenamento e pesquisa (não avaliaremos estética da documentação apenas clareza; use o tempo disponível no desenvolvimento do programa!).

## Avaliação

Os seguintes critérios serão avaliados

+ Uso eficiente do disco: o programa de armazenamento precisa se preocupar em usar o menor espaço em disco possível para os dados processados.
+ Busca eficiente dos dados: o programa de busca precisa se preocupar em trazer os resultados solicitados no menor tempo possível.
+ Qualidade do código (uso de boas práticas e adoção de um bom design).
+ Robustez das aplicações de armazenamento e pesquisa.
+ Existência de testes automatizados.

## Conclusão

Ao concluir o programa favor enviar a url de seu repositório para o endereço de email pelo qual estamos tratando das vagas de estágio.

